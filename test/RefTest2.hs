--
-- Copyright 2017 The Australian National University
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

{-# LANGUAGE OverloadedStrings #-}

import Foreign
import Foreign.C
import Foreign.C.String

import Mu.API
import Mu.AST
import Mu.PrettyPrint
import Mu.Impl.RefImpl2
import Mu.Interface

import System.IO.Unsafe (unsafePerformIO)
import Control.Monad.IO.Class (liftIO)

import qualified Data.Map.Strict as M
import qualified Data.Set as S

import Data.List (genericLength)
import Data.ByteString.Char8 (pack)

import qualified Mu.StdLib

import Mu.Test.RefTest2

-- This test mirrors `test_client2.c` from
-- https://gitlab.anu.edu.au/mu/mu-impl-ref2/blob/master/cbinding/test_client2.c

boot_image_name :: String
boot_image_name = "test_client2_bootimg.mu"

chkErr :: Ptr CInt -> String -> IO ()
chkErr ptr err = do
    errVal <- peek ptr
    case (fromEnum errVal) of
        0     -> return ()
        other -> error $ err ++ " (error status: " ++ (show other) ++ ")"
  
main :: IO ()
main = do
    gcOpts <- newCString "vmLog=DEBUG\nsosSize=524288\nlosSize=524288\nglobalSize=1048576\nstackSize=32768\n"
    putStrLn "Creating Micro VM..."
    mew <- newMu' gcOpts

    putStrLn "Creating client context..."
    ctx <- newContext mew
    errPtr <- getMuErrorPtr mew
    
    (names, _) <- withBuilder' ctx (M.empty, S.empty) $ mapM_ define bundle
    chkErr errPtr "Error loading bundle"
        
    -- The C version stores the IDs whereas in Haskell they die with the Builder monad,
    -- so we must keep a (M.Map String MuID) (see Interface.withBuilder') to retrieve them
    let string_t = lookup' "@string_t" names
    let g_hello_world = lookup' "@g_hello_world" names
    let main_func = lookup' "@main_func" names
    let i64 = lookup' "@i64" names
    let i32 = lookup' "@i32" names
    let ppi8 = lookup' "@ppi8" names
    
    v_hw_sz  <- handleFromSint64 ctx (fromIntegral $ length hw_string) 64
    v_hw_obj <- newHybrid ctx string_t v_hw_sz
    
    v_hw_uptr <- pin ctx v_hw_obj
    hw_uptr' <- handleToPtr ctx v_hw_uptr
    let hw_uptr = castPtr hw_uptr' :: Ptr Foreign.C.CChar
    strCpy hw_uptr (strToPtr hw_string)
    unpin ctx v_hw_obj
    
    v_g_hello_world <- handleFromGlobal ctx g_hello_world
    store ctx muOrdNotAtomic v_g_hello_world v_hw_obj
    
    v_main_func <- handleFromFunc ctx main_func
        
    v_main_thread_local <- newFixed ctx i64
    
    putStrLn "Making boot image..."
    
    
    let whitelist = [i64, main_func]
    whitelistArr <- Foreign.newArray whitelist
    let whitelistSz = genericLength whitelist
    makeBootImage ctx
        whitelistArr whitelistSz
        v_main_func Foreign.nullPtr v_main_thread_local
        Foreign.nullPtr Foreign.nullPtr 0
        Foreign.nullPtr Foreign.nullPtr 0
        (strToPtr boot_image_name)
        
    args <- mapM newCString ["Goodbye,", "cruel", "world!"]
    argv <- Foreign.newArray args
    let argc = genericLength args
    v_argc <- handleFromSint32 ctx (fromIntegral i32) argc
    v_argv <- handleFromPtr ctx ppi8 (castPtr argv :: MuCPtr)
    v_main_args <- Foreign.newArray [v_argc, v_argv]
    
    v_main_stack  <- newStack ctx v_main_func
    v_main_thread <- newThreadNor ctx v_main_stack Foreign.nullPtr v_main_args 2
    
    chkErr errPtr "Something went wrong"
    
    execute mew
    
    chkErr errPtr "Something went wrong during execution"
    
    _ <- closeMu mew
    
    return ()
