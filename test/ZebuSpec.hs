--
-- Copyright 2017 The Australian National University
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

{-# LANGUAGE OverloadedStrings #-}

import Foreign
import Foreign.C
import Foreign.C.String

import Mu.API
import Mu.AST
import Mu.PrettyPrint
import Mu.Impl.Fast
import Mu.Interface

import qualified Data.Map.Strict as M
import qualified Data.Set as S

import Data.List (genericLength)

import Mu.Test.Spec (bundle)

-- Zebu does not currently support exposed functions
-- Hence, remove last defintion from bundle
-- (see src/Mu/Test/Spec.hs)
bundle' :: [Definition]
bundle' = init bundle

lookup' :: Show a => Ord a => a -> (M.Map a b) -> b
lookup' key map = case (M.lookup key map) of
    Just val -> val
    Nothing  -> error $ "Unwrap failed for key: " ++ (show key)

main :: IO ()
main = do
    putStrLn "*** TEXT-FORM IR: ***"
    mapM_ (putStrLn . pp) bundle'
    putStrLn "******"

    putStrLn "Creating Micro VM"
    mew <- newMu

    putStrLn "Creating new context"
    ctx <- newContext mew

    putStrLn "Loading the bundle..."    
    (names, decls) <- withBuilder' ctx (M.empty, S.empty) $ do
        mapM_ define bundle'
        
    -- In VMs with AOT, making a boot image will force the IR to actually be compiled
    putStrLn "Making boot image..."
    let whitelist = map (\name -> lookup' name names) $ filter (
           \name -> (not $ elem '.' name) -- whitelist is all globals (i.e. no '.')
                && (name /= "@square_sum") )    -- don't whitelist @square_sum since it has no defined versions
                    (M.keys names)
    whitelistArr <- Foreign.newArray whitelist
    let whitelistSz = genericLength whitelist
    boot_image_name <- newCString "/dev/null" -- black hole; we just want to force it to AOT
    makeBootImage ctx
        whitelistArr whitelistSz
        Foreign.nullPtr Foreign.nullPtr Foreign.nullPtr
        Foreign.nullPtr Foreign.nullPtr 0
        Foreign.nullPtr Foreign.nullPtr 0
        boot_image_name

    return ()
