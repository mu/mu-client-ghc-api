--
-- Copyright 2017 The Australian National University
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

{-# LANGUAGE OverloadedStrings #-}

import Foreign
import Foreign.C
import Foreign.C.String

import Mu.API
import Mu.AST
import Mu.PrettyPrint
import Mu.Impl.RefImpl2
import Mu.Interface

import System.IO.Unsafe (unsafePerformIO)
import Control.Monad.IO.Class (liftIO)

import qualified Data.Map.Strict as M
import qualified Data.Set as S

import qualified Mu.StdLib

-- This test mirrors `test_client.c` from
-- https://gitlab.anu.edu.au/mu/mu-impl-ref2/blob/master/cbinding/test_client.c

chkErr :: Ptr CInt -> String -> IO ()
chkErr ptr err = do
    errVal <- peek ptr
    case (fromEnum errVal) of
        0     -> return ()
        other -> error $ err ++ " (error status: " ++ (show other) ++ ")" 

fromAddr :: Ptr a -> Integer
fromAddr = toInteger . fromEnum . ptrToIntPtr

strToPtr :: String -> Ptr Foreign.C.CChar
strToPtr = unsafePerformIO . newCString

foreign import ccall "unistd.h &write" writePtr :: Ptr ()
foreign import ccall "unistd.h strcpy" strCpy :: Ptr Foreign.C.CChar -> Ptr Foreign.C.CChar -> IO ()

hw_string :: String
hw_string = "Hello, world!\n"
hw2_string :: String
hw2_string = "Goodbye, cruel world!\n"

bundle1 :: [Definition]
bundle1 = [
    (TypeDefinition "@cint" (MuInt 32)),
    (TypeDefinition "@void" Void),
    (TypeDefinition "@cvoidptr" (UPtr "@void")),
    (TypeDefinition "@csize_t" (MuInt 64)),
    (SignatureDefinition "@write.sig" ["@cint", "@cvoidptr", "@csize_t"] ["@csize_t"]),
    (TypeDefinition "@write.fp" (UFuncPtr "@write.sig")),
    (Constant "@the_fd" "@cint" (IntCtor 1)),
    (TypeDefinition "@i64" (MuInt 64)),
    (Constant "@print_stat_point" "@i64" (IntCtor 92))]
    
bundle2 :: [Definition]
bundle2 = [
    (Constant "@the_write" "@write.fp" (IntCtor $ fromAddr writePtr)),
    (Constant "@the_string" "@cvoidptr" (IntCtor $ fromAddr $ strToPtr hw_string)),
    (Constant "@the_length" "@csize_t" (IntCtor $ toInteger $ length hw_string)),
    (Constant "@the_length2" "@csize_t" (IntCtor $ toInteger $ length hw2_string))]

bundle3 :: [Definition]
bundle3 = [
    (SignatureDefinition "@v_v" [] []),
    (FunctionDefinition "@hw" (Version "1") "@v_v" 
        (BasicBlock "@hw.1.entry"
            []
            Nothing
            [(["@hw.entry.rv"] := (CCall MuCallConvention "@write.fp" "@write.sig"
                "@the_write" ["@the_fd", "@the_string", "@the_length"] Nothing Nothing)),
            ([] := (Comminst CiUvmExtPrintStats [] [] [] ["@print_stat_point"] Nothing Nothing))]
            (Comminst CiUvmThreadExit [] [] [] [] Nothing Nothing))
             
         [])]
         
bundle4 :: [Definition]
bundle4 = [
    (TypeDefinition "@cchar" (MuInt 8)),
    (TypeDefinition "@charhybrid" (Hybrid [] "@cchar")),
    (TypeDefinition "@refvoid" (Ref "@void")),
    (FunctionDefinition "@hw2" (Version "1") "@v_v" 
        (BasicBlock "@hw2.1.entry"
            []
            Nothing
            [[] := (Comminst CiUvmExtClearStats [] [] [] [] Nothing Nothing),
             (["@hw2.1.entry.t1"] := (Comminst CiUvmGetThreadlocal [] [] [] [] Nothing Nothing)),
             (["@hw2.1.entry.p"]  := (Comminst CiUvmNativePin [] ["@refvoid"] [] ["@hw2.1.entry.t1"]
                Nothing Nothing)),
             (["@hw2.1.entry.rv"] := (CCall MuCallConvention "@write.fp" "@write.sig" "@the_write"
                ["@the_fd", "@hw2.1.entry.p", "@the_length2"] Nothing Nothing)),
             ([] := (Comminst CiUvmNativeUnpin [] ["@refvoid"] [] ["@hw2.1.entry.t1"] Nothing Nothing))]
             (Comminst CiUvmThreadExit [] ["@refvoid"] [] ["@hw2.1.entry.t1"] Nothing Nothing))
        
        [])]

main :: IO ()
main = do
    gcOpts <- newCString "vmLog=DEBUG\nsosSize=524288\nlosSize=524288\nglobalSize=1048576\nstackSize=32768\n"
    putStrLn "Creating Micro VM..."
    mew <- newMu' gcOpts

    putStrLn "Creating client context..."
    ctx <- newContext mew
    errPtr <- getMuErrorPtr mew

    (names1, decls1) <- withBuilder' ctx (M.empty, S.empty) $ mapM_ define bundle1
    chkErr errPtr "Loading bundle1 failed"
    
    (names2, decls2) <- withBuilder' ctx (names1, decls1) $ mapM_ define bundle2
    chkErr errPtr "Loading bundle2 failed"
    
    (names3, decls3) <- withBuilder' ctx (names2, decls2) $ mapM_ define bundle3
    chkErr errPtr "Loading bundle3 failed"
        
    putStrLn "Bundles loaded. Execute..."
    
    hwID <- ctxIdOf ctx $ strToPtr "@hw"
    func   <- handleFromFunc ctx hwID
    stack  <- newStack ctx func
    thread <- newThreadNor ctx stack Foreign.nullPtr Foreign.nullPtr 0
    execute mew
    chkErr errPtr "Something went wrong during execution"
    
    putStrLn "Loading additional bundle..."
    (names4, decls4) <- withBuilder' ctx (names3, decls3) $ mapM_ define bundle4
    chkErr errPtr "Loading bundle4 failed"
    
    putStrLn "Bundle loaded. Create a thread-local string object..."
    hlen  <- handleFromSint32 ctx 256 32
    charhybridID <- ctxIdOf ctx $ strToPtr "@charhybrid"
    hobj  <- newHybrid ctx charhybridID hlen
    hpobj <- pin ctx hobj
    mustrbuf <- handleToPtr ctx hpobj
    strCpy (castPtr mustrbuf :: Ptr Foreign.C.CChar) (strToPtr hw2_string)
    unpin ctx hobj
    chkErr errPtr "Something went wrong"
    
    putStrLn "Object populated. Create thread with threadlocal and execute..."
    hw2ID <- ctxIdOf ctx $ strToPtr "@hw2"
    func2   <- handleFromFunc ctx hw2ID
    stack2  <- newStack ctx func2
    thread2 <- newThreadNor ctx stack2 hobj Foreign.nullPtr 0
    execute mew
    chkErr errPtr "Something went wrong during execution"
        
    putStrLn "Closing the context"
    _ <- closeMu mew

    return ()
