--
-- Copyright 2017 The Australian National University
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE TemplateHaskell #-}

module Mu.Interface
    ( -- * Top-level interface
      buildBundle
    , define
      -- * Builder monad
    , Builder
    , withBuilder
    , withBuilder'
    , query
    , queries
      -- * Utility functions
    , getID
    , withSymbol
    , withAnonymousSymbol
    , withSymbolIO
    ) where

import qualified Data.ByteString as B
import Data.Functor (void)
import Data.Monoid ((<>))
import Data.Maybe (isJust)
import qualified Data.Set as S
import Control.Monad (when)
import Control.Monad.State (MonadState, StateT, evalStateT, execStateT, gets, modify)
import Control.Monad.Reader (MonadReader)
import Control.Monad.IO.Class (MonadIO, liftIO)
import Data.List (genericLength)
import Foreign.C.String (newCString)
import qualified Foreign as F
import qualified Foreign.C as C
import qualified Data.Map.Strict as M

import Lens.Micro.Platform ((%=), Lens')

import Mu.API
import Mu.AST

import Debug.Trace


-------------------------------------------------- * Top-level interface

-- | Load the Haskell-level Mu 'Bundle' and transfer it to a new MicroVM
-- instance.
buildBundle :: F.Ptr MuCtx -> Bundle -> IO ()
buildBundle ctx bundle = withBuilder ctx $ mapM_ define (unBundle bundle)


doExpr :: Assigned Expression -> Builder MuFuncNode
doExpr (dests := expr) = do
    irb <- gets _irbPtr
    withAnonymousSymbol $ \sym -> case expr of
        BinaryOperation binOp binType binV1 binV2 execClause -> do
            dstID <- query $ head dests -- there will only be one destination
            let optr = fromEnum' binOp -- type of BinOp is an integer
            ty <- query binType
            opnd1 <- query binV1
            opnd2 <- query binV2
            exc_clause <- exceptionClause irb execClause
            newBinop irb sym dstID optr ty opnd1 opnd2 exc_clause
        CompareOperation cmpOp cmpType cmpV1 cmpV2 -> do
            traceM $ "Creating new Compare op at ID: " ++ (show sym)
            dstID <- query $ head dests
            let optr = fromEnum' cmpOp
            ty <- query cmpType
            opnd1 <- query cmpV1
            opnd2 <- query cmpV2
            newCmp irb sym dstID optr ty opnd1 opnd2
        ConvertOperation convOp convTypeSrc convTypeDest convV -> do
            dstID <- query $ head dests
            let optr = fromEnum' convOp
            from_ty <- query convTypeSrc
            to_ty <- query convTypeDest
            opnd <- query convV
            newConv irb sym dstID optr from_ty to_ty opnd
        AtomicRMWOperation aRMWIsPtr aRMWMemOrd aRMWOp aRMWType aRMWLoc aRMWOpnd aRMWExecClause -> do
            dstID <- query $ head dests
            let is_ptr = bool aRMWIsPtr
            let ord = fromEnum' aRMWMemOrd
            let optr = fromEnum' aRMWOp
            refTy <- query aRMWType
            loc <- query aRMWLoc
            opnd <- query aRMWOpnd
            exc_clause <- exceptionClause irb aRMWExecClause
            newAtomicrmw irb sym dstID is_ptr ord optr refTy loc opnd exc_clause
        CmpXchg cmpXchgIsPtr cmpXchgIsWeak cmpXchgMemOrdSucc cmpXchgMemOrdFail cmpXchgType cmpXchgLoc cmpXchgExpect cmpXchgDesired cmpXchgExecClause -> do
            value_result_id <- query $ head dests
            succ_result_id  <- query $ last dests
            let is_ptr = bool cmpXchgIsPtr
            let is_weak = bool cmpXchgIsWeak
            let ord_succ = fromEnum' cmpXchgMemOrdSucc
            let ord_fail = fromEnum' cmpXchgMemOrdFail
            refty <- query cmpXchgType
            loc <- query cmpXchgLoc
            expected <- query cmpXchgExpect
            desired <- query cmpXchgDesired
            exc_clause <- exceptionClause irb cmpXchgExecClause
            newCmpxchg irb sym value_result_id succ_result_id is_ptr is_weak ord_succ ord_fail refty loc expected desired exc_clause
        Fence fenceMemOrd -> do
            let ord = fromEnum' fenceMemOrd
            newFence irb sym ord
        New newType newExecClause -> do
            result_id <- query $ head dests
            allocty <- query newType
            exc_clause <- exceptionClause irb newExecClause
            newNew irb sym result_id allocty exc_clause
        NewHybrid newHybridType newHybridLenType newHybridLen newHybridExecClause -> do
            result_id <- query $ head dests
            allocty <- query newHybridType
            lenty <- query newHybridLenType
            length <- query newHybridLen
            exc_clause <- exceptionClause irb newHybridExecClause
            newNewhybrid irb sym result_id allocty lenty length exc_clause
        Alloca allocaType allocaExecClause -> do
            result_id <- query $ head dests
            allocty <- query allocaType
            exc_clause <- exceptionClause irb allocaExecClause
            newAlloca irb sym result_id allocty exc_clause
        AllocaHybrid allocaHybridType allocaHybridLenType allocaHybridLen allocaHybridExecClause -> do
            result_id <- query $ head dests
            allocty <- query allocaHybridType
            lenty <- query allocaHybridLenType
            length <- query allocaHybridLen
            exc_clause <- exceptionClause irb allocaHybridExecClause
            newAllocahybrid irb sym result_id allocty lenty length exc_clause
        Return retvals -> do
            (vals, n_vals) <- queries retvals
            traceM $ "vals pointer: " ++ (show vals)
            newRet irb sym vals n_vals
        Throw throwException -> do
            exc <- query throwException
            newThrow irb sym exc
        Call callSignature callCallee callArgList callExceptionClause callKeepAliveClause -> do
            (result_ids, n_result_ids) <- queries dests
            sig <- query callSignature
            callee <- query callCallee
            (args, nargs) <- queries callArgList
            exc_clause <- exceptionClause irb callExceptionClause
            keepalive_clause <- keepaliveClause irb callKeepAliveClause
            newCall irb sym result_ids n_result_ids sig callee args nargs exc_clause keepalive_clause
        CCall ccallCallConv ccallType ccallSig ccallCallee ccallArgList ccallExceptionClause ccallKeepAliveClause -> do
            (result_ids, n_result_ids) <- queries dests
            let callconv = fromCallConv ccallCallConv
            callee_ty <- query ccallType
            sig <- query ccallSig
            callee <- query ccallCallee
            (args, nargs) <- queries ccallArgList
            exc_clause <- exceptionClause irb ccallExceptionClause
            keepalive_clause <- keepaliveClause irb ccallKeepAliveClause
            newCcall irb sym result_ids n_result_ids callconv callee_ty sig callee args nargs exc_clause keepalive_clause
        TailCall tailCallSignature tailCallCallee tailCallArgList -> do
            sig <- query tailCallSignature
            callee <- query tailCallCallee
            (args, nargs) <- queries tailCallArgList
            newTailcall irb sym sig callee args nargs
        Branch1 branch1Destination -> do
            traceM $ "Generating a branch at symbol ID: " ++ (show sym)
            dest <- destClause irb branch1Destination
            newBranch irb sym dest
        Branch2 branch2Cond branch2BranchTrue branch2BranchFalse -> do
            cond <- query branch2Cond
            if_true <- destClause irb branch2BranchTrue
            if_false <- destClause irb branch2BranchFalse
            newBranch2 irb sym cond if_true if_false
        WatchPoint watchpointname watchpointId watchpointTypes watchpointdis watchpointena watchpointWpExec watchpointKeepAlive -> do
            let wpid = fromEnum' watchpointId
            (result_ids, _) <- queries dests
            (rettys, nretvals) <- queries watchpointTypes
            dis <- destClause irb watchpointdis
            ena <- destClause irb watchpointena
            exc <- m_exc
            keepalive_clause <- keepaliveClause irb watchpointKeepAlive
            newWatchpoint irb sym wpid result_ids rettys nretvals dis ena exc keepalive_clause
                where
                    m_exc = case watchpointWpExec of
                        Nothing -> return muNoID
                        Just (WPExceptionClause exc_destClause) -> destClause irb exc_destClause
        Trap trapName trapTypes trapExceptionClause trapKeepAlive -> do
            (result_ids, _) <- queries dests
            (rettys, nretvals) <- queries trapTypes
            exc_clause <- exceptionClause irb trapExceptionClause
            keepalive_clause <- keepaliveClause irb trapKeepAlive
            newTrap irb sym result_ids rettys nretvals exc_clause keepalive_clause
        WPBranch wpBranchId wpBranchDis wpBranchEna -> do
            let wpid = fromEnum' wpBranchId
            dis <- destClause irb wpBranchDis
            ena <- destClause irb wpBranchEna
            newWpbranch irb sym wpid dis ena
        Switch switchType switchOpnd switchDefault switchBlocks -> do
            opnd_ty <- query switchType
            opnd <- query switchOpnd
            default_dest <- destClause irb switchDefault
            let (switchCases, switchDests) = unzip switchBlocks
            (cases, _) <- queries switchCases
            dests' <- mapM (destClause irb) switchDests
            (dests, ncasedests) <- toArray dests'
            newSwitch irb sym opnd_ty opnd default_dest cases dests ncasedests
        SwapStack swapStackSwapee swapStackCurStackClause swapStackNewStackClause swapStackExecClause swapStackKeepAliveClause -> do
            (result_ids, n_result_ids) <- queries dests
            swappee <- query swapStackSwapee
            cur_stack_clause <- curStackClause irb swapStackCurStackClause
            new_stack_clause <- newStackClause irb swapStackNewStackClause
            exc_clause <- exceptionClause irb swapStackExecClause
            keepalive_clause <- keepaliveClause irb swapStackKeepAliveClause
            newSwapstack irb sym result_ids n_result_ids swappee cur_stack_clause new_stack_clause exc_clause keepalive_clause
        NewThread newThreadStack newThreadStackClause newThreadExceptionClause -> do
            result_id <- query $ head dests
            stack <- query newThreadStack
            threadlocal <- newStackClause irb newThreadStackClause
            new_stack_clause <- newStackClause irb newThreadStackClause
            exc_clause <- exceptionClause irb newThreadExceptionClause
            newNewthread irb sym result_id stack threadlocal new_stack_clause exc_clause
        Comminst inst flags tys sigs vars exception keepalive -> do
            (tyIDs, tyIDsSz) <- queries tys
            (varIDs, varIDsSz) <- queries vars
            (destIDs, destIDsSz) <- queries dests
            (flagsPtr, nflags) <- toArray $ map fromFlag flags
            (sigsPtr, nsigs) <- queries sigs
            exception' <- exceptionClause irb exception
            keepalive' <- keepaliveClause irb keepalive
            newComminst irb sym
                        destIDs destIDsSz
                        (fromEnum' inst)
                        flagsPtr nflags
                        tyIDs tyIDsSz
                        sigsPtr nflags
                        varIDs varIDsSz
                        exception'
                        keepalive'
        Load loadIsPtr loadMemOrd loadType loadLoc loadExecClause -> do
            result_id <- query $ head dests
            let is_ptr = bool loadIsPtr
            refty <- query loadType
            loc <- query loadLoc
            exc_clause <- exceptionClause irb loadExecClause
            newLoad irb sym result_id is_ptr memOrd' refty loc exc_clause
                where
                    memOrd' = case loadMemOrd of
                        Nothing -> muOrdNotAtomic
                        Just ord -> fromEnum' ord
        Store isPtr memOrd ty dest val exc -> do
            tyID <- query ty
            destID <- query dest
            valID <- query val
            excID <- exceptionClause irb exc
            newStore irb sym (bool isPtr) memOrd' tyID destID valID muNoID
          where
            memOrd' = case memOrd of
                Nothing -> muOrdNotAtomic
                Just ord -> fromEnum' ord
        ExtractValue structExtractType structExtractIndex structExtractStruct -> do
            result_id <- query $ head dests
            strty <- query structExtractType
            let index = (fromIntegral . fromEnum) structExtractIndex
            opnd <- query structExtractStruct
            newExtractvalue irb sym result_id strty index opnd
        InsertValue structInsertType structInsertIndex structInsertStruct structInsertNewVal -> do
            result_id <- query $ head dests
            strty <- query structInsertType
            let index = (fromIntegral . fromEnum) structInsertIndex
            opnd <- query structInsertStruct
            newVal <- query structInsertNewVal
            newInsertvalue irb sym result_id strty index opnd newVal
        ExtractElement arrExtractType arrExtractIndexType arrExtractOpnd arrExtractIndex -> do
            result_id <- query $ head dests
            indty <- query arrExtractIndexType
            seqty <- query arrExtractType
            opnd <- query arrExtractOpnd
            index <- query arrExtractIndex
            newExtractelement irb sym result_id seqty indty opnd index
        InsertElement arrInsertType arrInsertIndexType arrInsertOpnd arrInsertIndex arrInsertNewVal -> do
            result_id <- query $ head dests
            indty <- query arrInsertIndexType
            seqty <- query arrInsertType
            opnd <- query arrInsertOpnd
            index <- query arrInsertIndex
            newval <- query arrInsertNewVal
            newInsertelement irb sym result_id seqty indty opnd index newval
        ShuffleVector arrShuffleVType arrShuffleMaskType arrShuffleV1 arrShuffleV2 arrShuffleMask -> do
            result_id <- query $ head dests
            vecty <- query arrShuffleVType
            maskty <- query arrShuffleMaskType
            vec1 <- query arrShuffleV1
            vec2 <- query arrShuffleV2
            mask <- query arrShuffleMask
            newShufflevector irb sym result_id vecty maskty vec1 vec2 mask
        GetIRef ty from -> do
            tyID <- query ty
            fromID <- query from
            resID <- query $ head dests
            newGetiref irb sym resID tyID fromID
        GetFieldIRef isPtr ty index from -> do
            tyID <- query ty
            fromID <- query from
            resID <- query $ head dests  -- unsafe, I know
            newGetfieldiref irb sym resID (bool isPtr) tyID (fromIntegral index) fromID
        GetElemIRef getElemIRefPtr getElemIRefTypeOpnd getElemIRefTypeIndex getElemIRefOpnd getElemIRefIndex -> do
            result_id <- query $ head dests
            let is_ptr = bool getElemIRefPtr
            refty <- query getElemIRefTypeOpnd
            indty <- query getElemIRefTypeIndex
            opnd <- query getElemIRefOpnd
            index <- query getElemIRefIndex
            newGetelemiref irb sym result_id is_ptr refty indty opnd index
        ShiftIRef shiftIRefPtr shiftIRefTypeOpnd shiftIRefTypeIndex shiftIRefOpnd shiftIRefOffset -> do
            result_id <- query $ head dests
            let is_ptr = bool shiftIRefPtr
            refty <- query shiftIRefTypeOpnd
            offty <- query shiftIRefTypeIndex
            opnd <- query shiftIRefOpnd
            offset <- query shiftIRefOffset
            newShiftiref irb sym result_id is_ptr refty offty opnd offset
        GetVarPartIRef getVarPartIRefPtr getVarPartIRefTypeOpnd getVarPartIRefOpnd -> do
            result_id <- query $ head dests
            let is_ptr = bool getVarPartIRefPtr
            refty <- query getVarPartIRefTypeOpnd
            opnd <- query getVarPartIRefOpnd
            newGetvarpartiref irb sym result_id is_ptr refty opnd


emitBB :: FunctionVerName -> BasicBlock -> Builder MuID
emitBB fvname (BasicBlock n params exc instr term) = do
    sym <- query n -- We need to generate a symbol before doing anything else, in case the block refers to itself
    irb <- gets _irbPtr
    (paramIDs, _) <- toArray =<< mapM (newSymbol . fst) params
    (paramtyIDs, n_paramTyIDs) <- queries $ fmap snd params
    excID <- case exc of
        Just excParam -> newSymbol excParam
        Nothing -> return muNoID
    (insts, n_insts) <- toArray =<< mapM doExpr (instr ++ [[] := term])
    traceM $ "Creating new BB with name: (" ++ (show n) ++ ") and ID: (" ++ (show sym) ++ ")"
    newBb irb sym paramIDs paramtyIDs n_paramTyIDs excID insts n_insts
    return sym

-- | Load a 'Definition' into the Mu itself.
define :: Definition -> Builder ()
define defn = do
    irb <- gets _irbPtr
    case defn of

        TypeDefinition n ty -> query n >>= \sym -> case ty of
            MuInt len -> newTypeInt irb sym (fromIntegral len)
            MuFloat -> newTypeFloat irb sym
            MuDouble -> newTypeDouble irb sym
            Ref refty -> do
                reftyID <- query refty
                newTypeRef irb sym reftyID
            IRef refty -> do
                reftyID <- query refty
                newTypeIref irb sym reftyID
            WeakRef refty -> do
                reftyID <- query refty
                newTypeWeakref irb sym reftyID
            UPtr refty -> do
                reftyID <- query refty
                newTypeUptr irb sym reftyID
            Struct tys -> do
                (tyIDs, count) <- queries tys
                newTypeStruct irb sym tyIDs count
            Array arrayType arrayLen -> do
                elemty <- query arrayType
                let len = (fromIntegral . fromEnum) arrayLen
                newTypeArray irb sym elemty len
            Vector vectorType vectorLen -> do
                elemty <- query vectorType
                let len = (fromIntegral . fromEnum) vectorLen
                newTypeVector irb sym elemty len
            Hybrid fixedtys varty -> do
                (fixedtyIDs, n_fixedtyIDs) <- queries fixedtys
                vartyID <- query varty
                newTypeHybrid irb sym fixedtyIDs n_fixedtyIDs vartyID
            Void -> newTypeVoid irb sym
            ThreadRef -> newTypeThreadref irb sym
            StackRef -> newTypeStackref irb sym
            FrameCursorRef -> newTypeFramecursorref irb sym
            TagRef64 -> newTypeTagref64 irb sym
            FuncRef sig -> do
                sigID <- query sig
                newTypeFuncref irb sym sigID
            UFuncPtr sig -> do
                sigID <- query sig
                newTypeUfuncptr irb sym sigID

        SignatureDefinition n argtys rettys -> do
            (argtyIDs, n_argtyIDs) <- queries argtys
            (rettyIDs, n_rettyIDs) <- queries rettys
            query n >>= \sym ->
                newFuncsig irb sym argtyIDs n_argtyIDs rettyIDs n_rettyIDs
            return ()

        FunctionDefinition n ver sig entry body -> do
            define (FunctionDeclaration n sig)
        
            sigID <- query sig
            knownIDs <- gets _ids
            funcID <- query n
            (blockIDs, n_blockIDs) <- toArray =<< mapM (emitBB fvName) (entry:body)
            query fvName >>= \sym ->
                newFuncVer irb sym funcID blockIDs n_blockIDs
            return ()
          where
            fvName = versionedName n ver

        FunctionDeclaration n sig -> do
            knownIDs <- gets _ids
            sym <- query n
            sigID <- query sig
            
            decls <- gets _funcdecls
            when (not $ S.member sym decls) $ do
                newFunc irb sym sigID
                funcdecls <- gets _funcdecls
                modify (\v -> v { _funcdecls = S.insert sym funcdecls })

        Constant n ty ctor -> do
            tyID <- query ty
            query n >>= \sym -> case ctor of
                IntCtor val -> newConstInt irb sym tyID (fromIntegral val)
                FloatCtor val -> newConstFloat irb sym tyID (C.CFloat val)
                DoubleCtor val -> newConstDouble irb sym tyID (C.CDouble val)
                ListCtor cNames -> do
                    (elems, nelems) <- queries cNames
                    newConstSeq irb sym tyID elems nelems
                NullCtor -> newConstNull irb sym tyID
                ExternCtor str -> liftIO $ B.useAsCString str $ \cstr ->
                    newConstExtern irb sym tyID cstr

        GlobalCell n ty -> query n >>= \sym -> do
            tyID <- query ty
            newGlobalCell irb sym tyID

        ExposedFunction exposeName functionName callConvention cookieName -> do
            sym <- query exposeName
            func <- query functionName
            let callconv = fromCallConv callConvention
            cookie <- query cookieName
            newExpFunc irb sym func callconv cookie


-------------------------------------------------- * Monad helper

data BuilderState = BuilderState
    { _ids :: M.Map String MuID
    , _ctxPtr :: F.Ptr MuCtx
    , _irbPtr :: F.Ptr MuIRBuilder
    , _funcdecls :: S.Set MuID
    }

ids :: Lens' BuilderState (M.Map String MuID)
ids f (BuilderState i cp ip decls) = fmap (\i' -> BuilderState i' cp ip decls) (f i)

newtype Builder a = Builder (StateT BuilderState IO a)
    deriving (Functor, Applicative, Monad, MonadState BuilderState, MonadIO)

withBuilder :: F.Ptr MuCtx -> Builder () -> IO ()
withBuilder ctx (Builder m) = do
    irb <- newIrBuilder ctx
    evalStateT m (BuilderState M.empty ctx irb S.empty)
    irbuilderLoad irb
    
withBuilder' :: F.Ptr MuCtx -> (M.Map String MuID, S.Set MuID) -> Builder () -> IO (M.Map String MuID, S.Set MuID)
withBuilder' ctx (names, decls') (Builder m) = do
    irb <- newIrBuilder ctx
    (BuilderState knownIDs _ _ decls) <- execStateT m (BuilderState names ctx irb decls')
    irbuilderLoad irb
    return (knownIDs, decls) -- return new map of known symbols

-- | Look up the value of a name.  If it doesn't exist, create it.
query :: HasName a => a -> Builder MuID
query n = do
    knownIDs <- gets _ids
    case M.lookup (nameOf n) knownIDs of
        Nothing -> newSymbol n
        Just i -> return i

queries :: HasName a => [a] -> Builder (F.Ptr MuID, MuArraySize)
queries ns = do
    ctx <- gets _ctxPtr
    ids <- mapM query ns
    toArray ids

remember :: HasName a => a -> MuID -> Builder ()
remember n i = trace ((show $ nameOf n) ++ " has ID: " ++ (show i)) ids %= M.insert (nameOf n) i


-------------------------------------------------- * Utility functions

toArray :: (F.Storable a, MonadIO m) => [a] -> m (F.Ptr a, MuArraySize)
toArray xs = do
    arr <- liftIO (F.newArray xs)
    return (arr, genericLength xs)


-- | Extract the 'MuID' of an object via its name as a @String@.
getID :: (HasName a, MonadIO m) => F.Ptr MuCtx -> a -> m (MuID)
getID ctx n = liftIO $ C.newCString (nameOf n) >>= ctxIdOf ctx


-- | Declare a symbol in the Mu.
newSymbol :: HasName a => a -> Builder MuID
newSymbol n = do
    irb <- gets _irbPtr
    n' <- liftIO (C.newCString (nameOf n))
    traceM $ "Generating new symbol (via newSymbol) for name: " ++ (show $ nameOf n)
    sym <- genSym irb n'
    remember n sym
    return sym


-- | Declare a symbol in the Mu, and return that symbol's ID.
withSymbol :: HasName a => a -> (MuID -> Builder b) -> Builder MuID
withSymbol n f = do
    irb <- gets _irbPtr
    n' <- liftIO (C.newCString (nameOf n))
    traceM $ "Generating new symbol (via withSymbol) for name: " ++ (show $ nameOf n)
    sym <- genSym irb n'
    remember n sym
    _ <- f sym -- If `f` refers to `sym`, we MUST remember the symbol before calling `f`
    return sym

-- | Declare a symbol in the Mu without a name attached to it, and return that
-- symbol's ID.
withAnonymousSymbol :: (MuID -> Builder b) -> Builder MuID
withAnonymousSymbol f = do
    irb <- gets _irbPtr
    sym <- genSym irb F.nullPtr
    traceM $ "Got anonymous symbol ID: " ++ (show sym)
    result <- f sym
    return sym

-- | Generalised version of 'withSymbol'.
withSymbolIO
    :: (HasName a, MonadIO m)
    => F.Ptr MuIRBuilder -> a -> (MuID -> m b) -> m MuID
withSymbolIO irb n f = do
    n' <- liftIO (C.newCString (nameOf n))
    sym <- genSym irb n'
    _ <- f sym
    return sym

bool :: Bool -> MuBool
bool b = case b of
    True -> 1
    False -> 0

fromCallConv :: CallConvention -> MuCallConv  
fromCallConv conv = case conv of 
    MuCallConvention -> 0 :: MuFlag
    ForeignCallConvention str -> error "Foreign call conventions not implemented!"
    
fromFlag :: Flag -> MuFlag
fromFlag (Flag flagValue) = case flagValue of
    "DEFAULT" -> 0 :: MuFlag
    _ -> error "Invalid flag!" -- TODO: Maybe handle this more gracefully
    
destClause :: F.Ptr MuIRBuilder -> DestinationClause -> Builder MuDestClause
destClause irb clause = withAnonymousSymbol $ \destClauseSymbol -> do
    traceM $ "Generating destination clause at ID: " ++ (show destClauseSymbol)
    let (DestinationClause destClauseDestination destClauseArgList) = clause
    dest <- query destClauseDestination
    (vars, nvars) <- queries destClauseArgList
    newDestClause irb destClauseSymbol dest vars nvars
    return destClauseSymbol
    
curStackClause :: F.Ptr MuIRBuilder -> CurStackClause -> Builder MuCurStackClause
curStackClause irb clause = withAnonymousSymbol $ \curStackSymbol -> case clause of
    KillOld -> do
        newCscKillOld irb curStackSymbol
        return curStackSymbol
    RetWith retWithTypes -> do
        (rettys, nrettys) <- queries retWithTypes
        newCscRetWith irb curStackSymbol rettys nrettys
        return curStackSymbol
        
newStackClause :: F.Ptr MuIRBuilder -> NewStackClause -> Builder MuCurStackClause
newStackClause irb clause = withAnonymousSymbol $ \newStackSymbol -> case clause of
    PassValues newStackTypes newStackValues -> do
        (tys, ntysvars) <- queries newStackTypes
        (vars, _) <- queries newStackValues
        newNscPassValues irb newStackSymbol tys vars ntysvars
        return newStackSymbol
    ThrowExc throwExecExceptionClause -> do
        exc <- query throwExecExceptionClause
        newNscThrowExc irb newStackSymbol exc
        return newStackSymbol
    
exceptionClause :: F.Ptr MuIRBuilder -> Maybe ExceptionClause -> Builder MuExcClause
exceptionClause irb exc = case exc of
    Just exc' -> withAnonymousSymbol $ \excClauseSymbol -> do
        let (ExceptionClause exceptionNor exceptionExc) = exc'
        norC <- destClause irb exceptionNor
        excC <- destClause irb exceptionExc
        newExcClause irb excClauseSymbol norC excC
        return excClauseSymbol
    Nothing -> return muNoID
    
keepaliveClause :: F.Ptr MuIRBuilder -> Maybe KeepAliveClause -> Builder MuKeepaliveClause
keepaliveClause irb kal = case kal of
    Just kal' -> withAnonymousSymbol $ \kalClauseSymbol -> do
        let (KeepAlive kalVars) = kal'
        (vars, nvars) <- queries kalVars
        newKeepaliveClause irb kalClauseSymbol vars nvars
        return kalClauseSymbol
    Nothing -> return muNoID

fromEnum' :: Enum a => a -> C.CUInt
fromEnum' = fromIntegral . fromEnum
