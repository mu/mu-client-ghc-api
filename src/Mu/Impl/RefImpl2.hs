--
-- Copyright 2017 The Australian National University
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

{-# LANGUAGE ForeignFunctionInterface #-}
module Mu.Impl.RefImpl2
    ( newMu
    , newMu'
    , closeMu
    ) where

import Data.Functor (void)
import Foreign.Ptr
import Foreign.C.Types

import Mu.API (MuVM)

foreign import ccall "refimpl2-start.h mu_refimpl2_new" newMu
    :: IO (Ptr MuVM)
foreign import ccall "refimpl2-start.h mu_refimpl2_new_ex" newMu'
    :: Ptr CChar -> IO (Ptr MuVM)
foreign import ccall "refimpl2-start.h mu_refimpl2_close" _close_mu
    :: Ptr MuVM -> IO (Ptr MuVM)

closeMu :: Ptr MuVM -> IO ()
closeMu = void . _close_mu
