--
-- Copyright 2017 The Australian National University
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

{-#LANGUAGE NoImplicitPrelude, ForeignFunctionInterface#-}

module Mu.StdLib (
  getcharAddr,
  putcharAddr,
  readAddr,
  writeAddr
              ) where

import Prelude (IO, String, fromIntegral)
import Foreign
import Foreign.C.Types
import Text.Printf

foreign import ccall "stdio.h &putchar" putchar :: FunPtr (CInt -> IO Word32)
foreign import ccall "stdio.h &getchar" getchar :: FunPtr (IO Word32)

foreign import ccall "unistd.h &read" read :: FunPtr (CInt -> Ptr () -> CSize -> IO Word64)
foreign import ccall "unistd.h &write" write :: FunPtr (CInt -> Ptr () -> CSize -> IO Word64)

getcharAddr :: String
getcharAddr = addressOf getchar

putcharAddr :: String
putcharAddr = addressOf putchar

readAddr :: String
readAddr = addressOf read

writeAddr :: String
writeAddr = addressOf write

addressOf :: FunPtr a -> String
addressOf fn = printf "0x%016x" (fromIntegral (ptrToIntPtr (castFunPtrToPtr fn)) :: Int)
