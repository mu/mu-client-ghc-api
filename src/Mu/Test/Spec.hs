--
-- Copyright 2017 The Australian National University
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

{-# LANGUAGE OverloadedStrings #-}

module Mu.Test.Spec (bundle) where

import Mu.AST

bundle :: [Definition]
bundle = [
    -- Encodes the first example in `ir.rst`
    -- https://gitlab.anu.edu.au/mu/mu-spec/blob/master/ir.rst
    (TypeDefinition "@i64" (MuInt 64)),
    (TypeDefinition "@double" MuDouble),
    (TypeDefinition "@void" Void),
    (TypeDefinition "@refvoid" (Ref "@void")),
        
    (Constant "@i64_0" "@i64" (IntCtor 0)),
    (Constant "@answer" "@i64" (IntCtor 42)),
    
    (TypeDefinition "@some_global_data_t" (Struct ["@i64", "@double", "@refvoid"])),
    (GlobalCell "@some_global_data" "@some_global_data_t"),
        
    (SignatureDefinition "@BinaryFunc" ["@i64", "@i64"] ["@i64"]),
        
    (FunctionDeclaration "@square_sum" "@BinaryFunc"),
        
    -- (@i64) -> (@i64) id function to debug failures
    (SignatureDefinition "@id_sig" ["@i64"] ["@i64"]),
        
    (FunctionDefinition "@id" (Version "v1") "@id_sig"
        (BasicBlock "@id.v1.entry"
            [("@id.v1.entry.param", "@i64")]
            Nothing
            []
            (Return ["@id.v1.entry.param"]))
         --(Comminst CiUvmThreadExit [] [] [] [] Nothing Nothing))
             
         []),
        
    -- Infinite loop function to debug failures
    (FunctionDefinition "@infi" (Version "v1") "@id_sig"
        (BasicBlock "@infi.v1.entry"
            [("@infi.v1.entry.param", "@i64")]
            Nothing
            []
            (Branch1 (DestinationClause "@infi.v1.head" ["@infi.v1.entry.param"])))
             
         [(BasicBlock "@infi.v1.head"
            [("@infi.v1.head.param", "@i64")]
            Nothing
            []

            (Branch1 (DestinationClause "@infi.v1.head" ["@infi.v1.head.param"])))]),
        
    (FunctionDefinition "@gcd" (Version "v1") "@BinaryFunc"
        (BasicBlock "@gcd.v1.entry" -- name
            [("@gcd.v1.entry.a", "@i64"), ("@gcd.v1.entry.b", "@i64")] -- parameters
            Nothing -- exception parameter
            [] -- instructions (Assigned Expression) (name := expression)
            (Branch1 (DestinationClause "@gcd.v1.head" ["@gcd.v1.entry.a", "@gcd.v1.entry.b"]))) -- terminating instruction (Expression)
                
        [(BasicBlock "@gcd.v1.head"
            [("@gcd.v1.head.a", "@i64"), ("@gcd.v1.head.b", "@i64")]
            Nothing
            [["@gcd.v1.head.z"] := (CompareOperation Mu.AST.EQ "@i64" "@gcd.v1.head.b" "@i64_0")]
            (Branch2 "@gcd.v1.head.z" (DestinationClause "@gcd.v1.exit" ["@gcd.v1.head.a"]) (DestinationClause "@gcd.v1.body" ["@gcd.v1.head.a", "@gcd.v1.head.b"]))),
            
         (BasicBlock "@gcd.v1.body"
            [("@gcd.v1.body.a", "@i64"), ("@gcd.v1.body.b", "@i64")]
            Nothing
            [["@gcd.v1.body.b1"] := (BinaryOperation SRem "@i64" "@gcd.v1.body.a" "@gcd.v1.body.b" Nothing)]
            (Branch1 (DestinationClause "@gcd.v1.head" ["@gcd.v1.body.b", "@gcd.v1.body.b1"]))),
             
         (BasicBlock "@gcd.v1.exit"
            [("@gcd.v1.exit.a", "@i64")]
            Nothing
            []
            (Return ["@gcd.v1.exit.a"]))]),
                
    (ExposedFunction "@gcd_native" "@gcd" MuCallConvention "@i64_0") ]
