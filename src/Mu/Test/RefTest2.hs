--
-- Copyright 2017 The Australian National University
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

{-# LANGUAGE OverloadedStrings #-}

-- This test mirrors `test_client2.c` from
-- https://gitlab.anu.edu.au/mu/mu-impl-ref2/blob/master/cbinding/test_client2.c
-- This file has some defintions common to both the Holstein and Zebu tests

module Mu.Test.RefTest2 where

{-# LANGUAGE OverloadedStrings #-}

import Foreign
import Foreign.C
import Foreign.C.String

import Mu.API
import Mu.AST
import Mu.PrettyPrint
import Mu.Impl.Fast
import Mu.Interface

import System.IO.Unsafe (unsafePerformIO)
import Control.Monad.IO.Class (liftIO)

import qualified Data.Map.Strict as M
import Data.List (genericLength)
import Data.ByteString.Char8 (pack)

import qualified Mu.StdLib

lookup' :: Show a => Ord a => a -> (M.Map a b) -> b
lookup' key map = case (M.lookup key map) of
    Just val -> val
    Nothing  -> error $ "Unwrap failed for key: " ++ (show key)

fromAddr :: Ptr a -> Integer
fromAddr = toInteger . fromEnum . ptrToIntPtr

strToPtr :: String -> Ptr Foreign.C.CChar
strToPtr = unsafePerformIO . newCString

foreign import ccall "unistd.h &write" writePtr :: Ptr ()
foreign import ccall "unistd.h strcpy" strCpy :: Ptr Foreign.C.CChar -> Ptr Foreign.C.CChar -> IO ()

hw_string :: String
hw_string = "**************\n"
         ++ "*Hello world!*\n"
         ++ "**************\n"

bundle :: [Definition]
bundle = [
    (TypeDefinition "@i1" (MuInt 1)),
    (TypeDefinition "@i8" (MuInt 8)),
    (TypeDefinition "@i32" (MuInt 32)),
    (TypeDefinition "@i64" (MuInt 64)),
    (TypeDefinition "@pi8" (UPtr "@i8")),
    (TypeDefinition "@ppi8" (UPtr "@pi8")),
    
    (TypeDefinition "@string_t" (Hybrid [] "@i8")),
    (TypeDefinition "@string_r" (Ref "@string_t")),
    (TypeDefinition "@string_p" (UPtr "@string_t")),
    
    (GlobalCell "@g_hello_world" "@string_r"),
    
    (SignatureDefinition "@main_sig" ["@i32", "@ppi8"] []),
    
    (TypeDefinition "@muvoid" Void),
    (TypeDefinition "@pvoid" (UPtr "@muvoid")),
    
    (SignatureDefinition "@write_sig" ["@i32", "@pvoid", "@i64"] ["@i64"]),
    (TypeDefinition "@write_fp" (UFuncPtr "@write_sig")),
    
    (Constant "@const_fp_write" "@write_fp" (ExternCtor $ pack "write")),
    
    (GlobalCell "@exit_status" "@i32"),
    
    (FunctionDeclaration "@main_func" "@main_sig"),
    
    (Constant "@const_i32_1" "@i32" (IntCtor 1)),
    (Constant "@const_i64_hwsz" "@i64" $ IntCtor ((toInteger $ length hw_string) + 1)),
    
    -- These are globals despite being amongst basic-block-building code in the C version
    (Constant "@const_i32_0" "@i32" (IntCtor 0)),
    
    (FunctionDefinition "@main_func" (Version "1") "@main_sig" 
        (BasicBlock "@main_func.1.entry"
            [("@p_argc", "@i32"), ("@p_argv", "@ppi8")]
            Nothing
            
            [["@main_func.1.entry.hw_ref"] := (Load False Nothing "@string_r" "@g_hello_world"
                Nothing),
             ["@main_func.1.entry.hw_ptr"] := (Comminst CiUvmNativePin [] ["@string_r"] []
                ["@main_func.1.entry.hw_ref"] Nothing Nothing),
             ["@main_func.1.entry.var_x"] := (ConvertOperation PTRCAST "@string_p" "@pvoid"
                "@main_func.1.entry.hw_ptr"),
             ["@main_func.1.entry.var_y"] := (CCall MuCallConvention "@write_fp" "@write_sig"
                "@const_fp_write" ["@const_i32_1", "@main_func.1.entry.var_x", "@const_i64_hwsz"]
                    Nothing Nothing),
             [] := (Comminst CiUvmNativeUnpin [] ["@string_r"] [] ["@main_func.1.entry.hw_ref"]
                Nothing Nothing),
             [] := (Store False Nothing "@i32" "@exit_status" "@const_i32_0" Nothing)]
            
            (Comminst CiUvmThreadExit [] [] [] [] Nothing Nothing))
        
        [])]
